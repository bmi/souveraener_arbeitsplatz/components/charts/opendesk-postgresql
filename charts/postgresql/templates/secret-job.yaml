{{/*
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
*/}}
{{- if .Values.job.enabled }}
---
apiVersion: "v1"
kind: "Secret"
metadata:
  name: "{{ include "common.names.fullname" . }}-job"
  namespace: {{ include "common.names.namespace" . | quote }}
  labels: {{- include "common.labels.standard" . | nindent 4 }}
    {{- if .Values.commonLabels }}
    {{- include "common.tplvalues.render" ( dict "value" .Values.commonLabels "context" . ) | nindent 4 }}
    {{- end }}
  {{- if .Values.commonAnnotations }}
  annotations: {{- include "common.tplvalues.render" ( dict "value" .Values.commonAnnotations "context" . ) | nindent 4 }}
  {{- end }}
stringData:
  init-wait.sh: |
    RETRIES={{ .Values.job.retries }}
    until psql -c "select 1" > /dev/null 2>&1 || [ $RETRIES -eq "0" ]; do
      echo "Waiting {{ .Values.job.wait }}s for postgres server, $((RETRIES--)) remaining attempts..."
      sleep {{ .Values.job.wait }}
    done
    if [ ${RETRIES} -eq "0" ]; then
      echo "Maximum wait attempts exceeded, aborting".
      exit 1;
    fi
  init-user.sh: |
    CURRENT_MAX_CONNECTIONS=$(psql -t -c "SHOW max_connections;" | xargs)
    echo "Current set max_connections ($CURRENT_MAX_CONNECTIONS)."
    {{- $maxConnectionSum := 15 -}}
    {{- range .Values.job.users }}
    echo "Working on {{ tpl .username $ }}..."
    if psql -tc "SELECT 1 FROM pg_user WHERE usename = '{{ tpl .username $ }}'" | grep -q 1; then
      echo "User already exists, updating."
      psql -c {{ printf "ALTER USER %s WITH ENCRYPTED PASSWORD %s CONNECTION LIMIT %s" ( .username | quote ) ( tpl .password $ | replace "'" "''" | squote ) ( .connectionLimit | toString ) | quote }};
    else
      echo "User does not exist, creating."
      psql -c {{ printf "CREATE USER %s WITH ENCRYPTED PASSWORD %s CONNECTION LIMIT %s" ( .username | quote ) ( tpl .password $ | replace "'" "''" | squote ) ( .connectionLimit | toString ) | quote }};
    fi
    {{- $maxConnectionSum = add $maxConnectionSum .connectionLimit }}
    {{- end }}
    echo "Final required max_connections: {{ $maxConnectionSum }}"
    if [[ "$CURRENT_MAX_CONNECTIONS" -ne "{{ $maxConnectionSum }}" ]]; then
      echo "Updating required limits from current $CURRENT_MAX_CONNECTIONS to {{ $maxConnectionSum }}"
      psql -c {{ printf "ALTER SYSTEM SET MAX_CONNECTIONS = %s" ( $maxConnectionSum | toString ) | quote }};
      # echo "Restarting the postgres Service."
      # pg_ctl restart
    fi
  init-db.sh: |
    {{- range .Values.job.databases }}
    psql -tc "SELECT 1 FROM pg_database WHERE datname = '{{ tpl .name $ }}'" | grep -q 1 || psql -c "CREATE DATABASE {{ tpl .name $ }} {{ with .additionalParams }}{{ . }} {{ end }}";
    psql -c "ALTER DATABASE {{ tpl .name $ }} OWNER TO {{ tpl .user $ }}";
    psql -c "GRANT ALL PRIVILEGES ON DATABASE {{ tpl .name $ }} TO {{ tpl .user $ }}";
    {{- end }}
...
{{- end }}
